package com.epam.esm.repository;

import com.epam.esm.model.Tag;

import java.util.List;
public interface TagDao {
    Tag findById(Long id);
    Tag findByName(String name);
    void create(Tag tag);
    void delete(Long id);

}
