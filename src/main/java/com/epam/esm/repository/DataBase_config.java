package com.epam.esm.repository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
public class DataBase_config {

    private static final String DATABASE_URL = "AQUI ENTIENDO QUE VA LA CONEXION CON EL SERVER QUE HAGA DE MYSQL?";
    private static final String DATABASE_USERNAME = "username";
    private static final String DATABASE_PASSWORD = "password";

    public static void createTables() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource(DATABASE_URL, DATABASE_USERNAME, DATABASE_PASSWORD);
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.execute("DROP TABLE IF EXISTS gift_certificate_tag;");
        jdbcTemplate.execute("DROP TABLE IF EXISTS gift_certificate;");
        jdbcTemplate.execute("DROP TABLE IF EXISTS tag;");

        jdbcTemplate.execute("CREATE TABLE gift_certificate (id SERIAL PRIMARY KEY, name VARCHAR(255), description VARCHAR(255), "
                + "price NUMERIC(10, 2), duration INTERVAL, create_date TIMESTAMP, last_update_date TIMESTAMP);");

        jdbcTemplate.execute("CREATE TABLE tag (id SERIAL PRIMARY KEY, name VARCHAR(255));");

        jdbcTemplate.execute("CREATE TABLE gift_certificate_tag (gift_certificate_id BIGINT, tag_id BIGINT, "
                + "FOREIGN KEY (gift_certificate_id) REFERENCES gift_certificate (id) ON DELETE CASCADE, "
                + "FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE, "
                + "PRIMARY KEY (gift_certificate_id, tag_id));");
    }

}
