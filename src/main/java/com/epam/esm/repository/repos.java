package com.epam.esm.repository;

import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;

import java.util.List;

public interface repos {

    public interface GiftCertificateRepository {
        GiftCertificate findById(Long id);
        List<GiftCertificate> findAll();
        void create(GiftCertificate giftCertificate);
        void update(GiftCertificate giftCertificate);
        void delete(Long id);
    }

    public interface TagRepository {
        Tag findById(Long id);
        Tag findByName(String name);
        void create(Tag tag);
        void delete(Long id);
    }
}
