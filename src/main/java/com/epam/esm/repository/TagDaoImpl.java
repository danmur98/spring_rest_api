package com.epam.esm.repository;

import com.epam.esm.model.Tag;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagDaoImpl implements TagDao {

    private JdbcTemplate jdbcTemplate;

    public TagDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public Tag findById(Long id) {
        return null;
    }

    @Override
    public Tag findByName(String name) {
        return null;
    }

    @Override
    public void create(Tag tag) {

    }

    @Override
    public void delete(Long id) {

    }
}
