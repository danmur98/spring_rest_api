package com.epam.esm.repository;

import com.epam.esm.model.GiftCertificate;

import java.util.List;
public interface GiftCertificateDao {

    GiftCertificate findById(Long id);
    List<GiftCertificate> findAll();
    void create(GiftCertificate giftCertificate);
    void update(GiftCertificate giftCertificate);
    void delete(Long id);
}
