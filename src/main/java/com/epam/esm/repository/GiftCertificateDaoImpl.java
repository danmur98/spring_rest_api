package com.epam.esm.repository;

import com.epam.esm.model.GiftCertificate;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class GiftCertificateDaoImpl implements GiftCertificateDao {

    private JdbcTemplate jdbcTemplate;

    public GiftCertificateDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public GiftCertificate findById(Long id) {
        return null;
    }

    @Override
    public List<GiftCertificate> findAll() {
        return null;
    }

    @Override
    public void create(GiftCertificate giftCertificate) {

    }

    @Override
    public void update(GiftCertificate giftCertificate) {

    }

    @Override
    public void delete(Long id) {

    }
}
